#include<stdio.h>
#include<string.h>


// Levenshtein's algorithm.
int levenshtein(char str1[], char str2[]);
// Simple min function.
int min(int a, int b);
// Pretty print matrix, for debugging.
void pretty_m_print(size_t m, size_t n, int d[m][n]);


int main(int argc, char *argv[])
{
  // Init empty char arrays.
  char string1[100];
  char string2[100];

  // Ask user for to input both strings.
  fprintf(stdout, "Input first string:\t");
  scanf("%s", string1);
  fprintf(stdout, "Input second string:\t");
  scanf("%s", string2);

  // Now compute the edit distance between strings.
  int edit_distance;
  edit_distance = levenshtein(string1, string2);

  // Print and return.
  fprintf(stdout, "Edit distance between %s and %s is %i\n", \
   string1, string2, edit_distance);
  return 0;
}


int levenshtein(char str1[], char str2[])
{
  // Levenshtein Algorithm: Pseudo-code from
  // https://people.cs.pitt.edu/~kirk/cs1501/Pruhs/Spring2006/assignments/editdistance/Levenshtein%20Distance.htm
  // First compute the length of both strings.
  int len_str1 = strlen(str1);
  int len_str2 = strlen(str2);

  // Then check if either string is the empty string.
  if (len_str1 == 0)
  {
    // If it is the empty string, we return the length of the other string.
    return len_str2;
  }
  else if (len_str2 == 0)
  {
    return len_str1;
  }

  // Now, init a matrix with sizes len_str1 x len_str2.
  int d[len_str1 + 1][len_str2 + 1];

  // Fill the first row and column with 1..lengths.
  for (int i = 0; i <= len_str1; i++)
  {
    d[i][0] = i;
  }
  for (int i = 0; i <= len_str2; i++)
  {
    d[0][i] = i;
  }

  // We loop over both strings from 1..lengths.
  for (int i = 1; i <= len_str1; i++)
  {
    for (int j = 1; j <= len_str2; j++)
    {
      // 1. Compute the cost: 0 is chars are same, 1 otherwise.
      int cost = (str1[i - 1] == str2[j - 1]) ? (0) : (1);
      // 2. Take the min of the cell above and the cell to the left + 1.
      int min_val = min(d[i - 1][j] + 1, d[i][j - 1] + 1);
      // 3. Take the min of min_val and the left-above cell + cost.
      min_val = min(min_val, (d[i - 1][j - 1] + cost));
      // 4. Fill min_val int d[i][j]
      d[i][j] = min_val;
    }
  }

  // TODO: Remove this after assessment.
  pretty_m_print(len_str1 + 1, len_str2 + 1, d);

  // Return the right-bottom element of d. This is the edit distance.
  return d[len_str1][len_str2];
}


int min(int a, int b)
{
  return (a < b) ? (a) : (b);
}


void pretty_m_print(size_t m, size_t n, int d[m][n])
{
  fprintf(stdout, "Pretty printing matrix d.\n");
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
    {
      fprintf(stdout, "%i |\t ", d[i][j]);
    }
    fprintf(stdout, "\n");
  }
  // Note we can use this for a trace of the solution: Follow the mins.
}
